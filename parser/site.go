package parser

import "net/url"

type News struct {
	Title       string
	Description string
	Image       string
}

func DetectSite(feedUrl string) string {
	u, err := url.Parse(feedUrl)
	if err != nil {
		panic(err)
	}

	return u.Host
}
