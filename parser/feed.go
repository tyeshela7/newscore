package parser

import (
	"github.com/mmcdole/gofeed/rss"
	"strings"
	"time"
)

const Delay = 60

func FetchFeed(rssFeed string) *rss.Feed {
	fp := rss.Parser{}
	feedItems, _ := fp.Parse(strings.NewReader(rssFeed))
	return feedItems
}

func GetNewItemsFromFeed(rssFeed *rss.Feed) []*rss.Item {
	var items []*rss.Item
	for _, Item := range rssFeed.Items {
		publishedAt, err := time.Parse(time.RFC1123, Item.PubDate)
		if err != nil {
			panic(err)
		}

		if publishedAt.After(time.Now().Add(-Delay * time.Second)) {
			items = append(items, Item)
		} else {
			break
		}
	}

	return items
}
