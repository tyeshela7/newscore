package parser

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

type Bbc struct {
	Data map[string]BbcData
}

type BbcData struct {
	Name string
	Data BbcArticleData
}

type BbcArticleData struct {
	Metadata BbcArticleMetadata
	Content  BbcArticleContent
	//Tags []string
}

type BbcArticleTags struct {
	Text string
}

type BbcArticleMetadata struct {
	IndexImage  BbcArticleImage
	SeoHeadline string
	Description string
}

type BbcArticleImage struct {
	OriginalSrc string
}

type BbcArticleContent struct {
	Model BbcArticleContentModel
}

type BbcArticleContentModel struct {
	Blocks []BbcArticleContentModelBlocks
}

type BbcArticleContentModelBlocks struct {
	Type  string
	Model BbcArticleContentModelBlocksModel
}

type BbcArticleContentModelBlocksModel struct {
	Caption BbcArticleContentModelBlocksModelAny
	Image   BbcArticleContentModelBlocksModelImage
	Blocks  []BbcArticleContentModelBlocksModelAny
}

type BbcArticleContentModelBlocksModelImage struct {
	Copyright string
	Src       string
}

type BbcArticleContentModelBlocksModelAny struct {
	Type  string
	Model BbcArticleContentModelBlocksModelAnyModel
}

type BbcArticleContentModelBlocksModelAnyModel struct {
	Text   string
	Blocks []BbcArticleContentModelBlocksModelAnyModelBlocks
}

type BbcArticleContentModelBlocksModelAnyModelBlocks struct {
	Type  string
	Model BbcArticleContentModelBlocksModelAnyModelBlocksModel
}

type BbcArticleContentModelBlocksModelAnyModelBlocksModel struct {
	Text       string
	Attributes []string
	Locator    string
}

func ProcessBbcJson() BbcData {
	jsonFile, err := os.Open("./data.json")
	if err != nil {
		panic(err)
	}
	defer func(jsonFile *os.File) {
		err := jsonFile.Close()
		if err != nil {
			panic(err)
		}
	}(jsonFile)

	byteValue, _ := ioutil.ReadAll(jsonFile)

	var wrapper Bbc

	err = json.Unmarshal(byteValue, &wrapper)

	var bbcData BbcData
	for _, item := range wrapper.Data {
		if item.Name == "article" {
			bbcData = item
			break
		}
	}
	return bbcData
}
