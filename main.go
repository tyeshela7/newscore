package main

import (
	"fmt"
	"newscore/parser"
)

func main() {
	data := parser.ProcessBbcJson()
	//save
	fmt.Println(data.Data.Metadata.IndexImage.OriginalSrc)

	fmt.Println(data.Data.Metadata.SeoHeadline)
	fmt.Println(data.Data.Metadata.Description)

	var text string
	for _, item := range data.Data.Content.Model.Blocks {
		if item.Type == "text" {
			for _, blockItem := range item.Model.Blocks {
				text = blockItem.Model.Text
				for _, bItem := range blockItem.Model.Blocks {
					if bItem.Type == "fragment" {
						if bItem.Model.Attributes != nil {
							for _, class := range bItem.Model.Attributes {
								text += " | " + class
							}
						}
					} else if bItem.Type == "urlLink" {
						text += " | " + bItem.Model.Locator
					}
				}
				fmt.Println(text)
			}
		}
		if item.Type == "image" {
			var cText string
			for _, caption := range item.Model.Caption.Model.Blocks {
				cText = caption.Model.Text
			}
			fmt.Println(item.Model.Image.Src + " | " + cText + " | " + item.Model.Image.Copyright)
		}
	}
}
